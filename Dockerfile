from ubuntu:latest

RUN apt update && apt install curl net-tools ca-certificates -y

ADD run.sh /run.sh

ENV TOKEN ""
ENV USER ""
ENV INTERVAL 600

CMD ["/run.sh"]
