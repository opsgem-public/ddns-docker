#!/bin/bash

echo Version: 0.0.1

while true
do
  IP=

  for i in $(/sbin/ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')
  do
    if [ "$IP" == "" ]
    then
      IP=$i
    else
      IP=$IP,$i
    fi
  done

  echo $IP

  /usr/bin/curl "http://api.opsgem.cloud/api/v1/workflow/run/qnap?user=${USER}&token=${TOKEN}&host=${HOSTNAME}&ip=${IP}"

  /bin/sleep $INTERVAL
done
